#! /usr/bin/python3

from gps import *
from PIL import Image, ImageDraw, ImageFont
import pandas
import time
import math

MPS_TO_MPH = 2.23694
HEIGHT     = 64
WIDTH      = 128
AVG_WINDOW = int(WIDTH/2)

NO_DISPLAY = 0


if (NO_DISPLAY == 0):
    import board
    import busio
    import adafruit_ssd1306
    i2c     = busio.I2C(board.SCL, board.SDA)
    display = adafruit_ssd1306.SSD1306_I2C(WIDTH, HEIGHT, i2c, addr=0x3C)

gps_connected = False

while (gps_connected == False):
    try:
        gpsd    = gps(mode=WATCH_ENABLE|WATCH_NEWSTYLE)
        gps_connected = True
    except (ConnectionRefusedError):
        gps_connected = False
        print ("Is GPSD running?")
        time.sleep(0.5)
    except (KeyboardInterrupt):
        print ("Keyboard Exit.")
        exit(0)

speed_mps     = 0.0
speed_mph     = 0.0
speed_err_mps = 0.0
speed_err_mph = 0.0
track         = 0.0
loop_cnt      = 1
alpha         = 0.01
average_speed = []
speed_list    = []

# Image and fonts
image         = Image.new("1", (WIDTH, HEIGHT))
draw          = ImageDraw.Draw(image)

if (NO_DISPLAY == 0):
    reg_tiny_font = ImageFont.truetype('/home/pi/car_comp/anonymous_pro_reg.ttf', 11)
    reg_font      = ImageFont.truetype('/home/pi/car_comp/anonymous_pro_reg.ttf', 16)
    bol_font      = ImageFont.truetype('/home/pi/car_comp/anonymous_pro_reg.ttf', 28)
else:
    reg_tiny_font = ImageFont.truetype('anonymous_pro_reg.ttf', 11)
    reg_font      = ImageFont.truetype('anonymous_pro_reg.ttf', 16)
    bol_font      = ImageFont.truetype('anonymous_pro_reg.ttf', 28)

# Functions
def initialize_display():
    display.fill(0)
    display.show()

def get_needle_coordinates(x1, y1, length, angle):
    radian_angle = math.pi * angle / 180
    x = x1 + (length * math.sin(radian_angle))
    y = y1  - (length * math.cos(radian_angle))
    return (x1, y1, x, y)

def draw_compass(x1, y1, x2, y2, needle_length, needle_angle):
    # Draw bounding box
    # draw.rectangle((x1, y1, x2, y2), outline=255, fill=0)
    draw.ellipse  ((x1, y1, x2, y2), outline=255, fill=0)

    # Draw Cardinal Points
    north_x = x1 + ((x2 - x1) / 2) - 2
    north_y = y1 + 2
    draw.text((north_x, north_y), "N", font=reg_tiny_font, fill=255)
    east_x  = x2 - 6
    east_y  = y1 + ((y2 - y1) / 2) - 4
    draw.text((east_x, east_y),   "E", font=reg_tiny_font, fill=255)
    south_x = x1 + ((x2 - x1) / 2) - 2
    south_y = y2 - 12
    draw.text((south_x, south_y), "S", font=reg_tiny_font, fill=255)
    west_x  = x1 + 4
    west_y  = y1 + ((y2 - y1) / 2) - 4
    draw.text((west_x, west_y),   "W", font=reg_tiny_font, fill=255)


    # Find needle center point and draw it
    nx1 = x1 + ((x2 - x1) / 2)
    ny1 = y1 + ((y2 - y1) / 2)
    draw.line(get_needle_coordinates(nx1, ny1, needle_length, needle_angle), fill=255)

def map_value(s_max, s_min, d_max, d_min, value):
    if ((s_max-s_min) == 0):
        return int(d_min + ((d_max - d_min) / 0.5) * (value - s_min))
    else:
        return int(d_min + ((float(d_max) - float(d_min)) / (float(s_max) - float(s_min))) * (value - s_min))

def draw_graph(x1, y1, x2, y2, speed_array, avg_array):
    width = int(x2-x1-2)
    if (len(speed_array) < width):
        width = len(speed_array)
    ymax  = max(speed_array[len(speed_array)-width:len(speed_array)])
    ymin  = min(speed_array[len(speed_array)-width:len(speed_array)])

    # Draw bounding box
    draw.rectangle((x1, y1, x2, y2), outline=255, fill=0)

    # Iterate and draw points
    for ii in range(width):
        if ((ymax - ymin) >= 2):
            draw.rectangle((x1+ii+1, map_value(ymax, ymin, y1+2, y2-2, speed_array[len(speed_array)-(width-ii)]),
                            x1+ii+2, map_value(ymax, ymin, y1+2, y2-2, speed_array[len(speed_array)-(width-ii)])-1), outline=255,
                            fill=255)
        else:
            draw.rectangle((x2-3, y2-3, x2-1, y2-1), outline=255, fill=255)
            draw.rectangle((x1+ii+1, map_value(ymax+1, ymin, y1+2, y2-2, speed_array[len(speed_array)-(width-ii)]),
                            x1+ii+2, map_value(ymax+1, ymin, y1+2, y2-2, speed_array[len(speed_array)-(width-ii)])-1), outline=255,
                            fill=255)

    # Draw AVG point
    tri_p1 = (x2-4, map_value(ymax, ymin, y1+2, y2-2, avg_array[len(avg_array)-1]))
    tri_p2 = (x2,   map_value(ymax, ymin, y1+2, y2-2, avg_array[len(avg_array)-1]) + 4)
    tri_p3 = (x2,   map_value(ymax, ymin, y1+2, y2-2, avg_array[len(avg_array)-1]) - 4)
    draw.polygon([tri_p1, tri_p2, tri_p3], outline=255, fill=255)

def draw_display(cur_speed, cur_track, cur_speed_err, speed_history, avg_speed):
    draw.rectangle((0, 0, WIDTH-1, HEIGHT-1),           outline=255, fill=0)
    draw.line     ((WIDTH/2-1, 0, WIDTH/2-1, HEIGHT-1), fill=255)
    draw.line     ((0, 11, WIDTH-1, 11),                fill=255)

    draw.text((4,  1), "{:<10s}".format ("AVG:{:4.1f}".format(avg_speed[len(avg_speed)-1])), font=reg_tiny_font, fill=255)
    draw.text((68, 1), "{:<10s}".format ("TRK:{:>5.1f}°".format(cur_track)), font=reg_tiny_font, fill=255)
    draw.text((2,  8), "{:^5.1f}".format(cur_speed), font=bol_font, fill=255)
    draw_graph(0, 34, WIDTH/2-1, HEIGHT-1, speed_history, avg_speed)

    # 48x48
    draw_compass (71, 13, WIDTH-9, HEIGHT-3, 16, cur_track)

    if (NO_DISPLAY == 0):
        display.image(image)
        display.show()
    else:
        image.save('display_sample.bmp', 'BMP')

# Begin
if (NO_DISPLAY == 0):
    initialize_display()

while True:
    try:
        report = gpsd.next()
        if report['class'] == 'TPV':
            if (getattr(report, 'mode', 0) >= 2):
                speed_mps = getattr(report, 'speed', 0.0)
                speed_mph = speed_mps * MPS_TO_MPH
                start_time = time.monotonic()
                speed_list.append(speed_mph)
                data_frame = pandas.Series(data=speed_list)
                average_speed = data_frame.rolling(AVG_WINDOW, min_periods=1)
                average_speed = average_speed.mean()

                if ((track != getattr(report, 'track', 0.0)) and (speed_mph >= 1.0)):
                    track = getattr(report, 'track', 0.0)
                if (speed_err_mps != getattr(report, 'eps', 0.0)):
                    speed_err_mps = getattr(report, 'eps', 0.0)
                    speed_err_mph = speed_err_mps * MPS_TO_MPH

                loop_cnt += 1
                if (loop_cnt >= AVG_WINDOW):
                    speed_list.pop(0)

                draw_display(speed_mph, track, speed_err_mph, speed_list, average_speed)

                stop_time = time.monotonic()
                avg_time = stop_time - start_time

                if ((len(average_speed) >= 1) and (NO_DISPLAY == 1)):
                    print ("track {:6.2f} | {:5.2f} mph | avg {:5.2f} mph | cnt {:d} | time {:f}".format(track,
                           speed_mph, average_speed[len(average_speed)-1], loop_cnt, avg_time))

            elif (getattr(report, 'mode', 0) <= 1):
                draw.text((1, 1), "No GPS Fix", font=reg_font, fill=255)
                if (NO_DISPLAY == 0):
                    display.image(image)
                    display.show()

    except (KeyboardInterrupt):
        print ("Keyboard Exit.")
        if (NO_DISPLAY == 0):
            image.save('display_sample.bmp', 'BMP')

        exit(0)

    except (StopIteration):
        print ("GPSD exited.")
        exit(-1)
